package com.servlet.controller;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by User on 5/30/2020.
 */
@WebFilter(urlPatterns = "/welcome")
public class WelcomeFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Integer id = Integer.parseInt(servletRequest.getParameter("id"));
        String name = servletRequest.getParameter("name");
        if(id>0)
        filterChain.doFilter(servletRequest,servletResponse);
        else{
            PrintWriter out = servletResponse.getWriter();
            out.println("Hello "+name+" your id is wrong. And This is from welFilter class");
        }
    }

    @Override
    public void destroy() {

    }
}
